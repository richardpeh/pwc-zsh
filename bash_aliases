# ### Add this to your default bashrc file, to load all my customized alias
#
# if [ -f ~/.bash_aliases ]; then
#     source ~/.bash_aliases
# fi


# ==================================================================================
# Setting
# ==================================================================================

# Flush History Immediately
export PROMPT_COMMAND='history -a'


# ==================================================================================
# General Alias
# ==================================================================================

alias l='ls'
alias ll='ls -lh'
alias la='ls -lAh'
alias hg='history | grep'

# ==================================================================================
# Python
# ==================================================================================

# PYTHON PIP
export PATH=$PATH:/Users/pwc/Library/Python/2.7/bin

# Setting PATH for Python 3.5
PATH="/Library/Frameworks/Python.framework/Versions/3.5/bin:${PATH}"
export PATH

# Setting PATH for Python 2.7
PATH="/Library/Frameworks/Python.framework/Versions/2.7/bin:${PATH}"
export PATH


# =================================================================================
# Kafka
# =================================================================================

# CONFLUENT
export CONFLUENT_HOME=/Users/pwc/code/mylib/confluent-5.4.0
export CONFLUENT_CLI=/Users/pwc/code/mylib/confluent-cli
export PATH=$PATH:$CONFLUENT_CLI:$CONFLUENT_HOME/bin

# ==================================================================================
# GIT Alias
# ==================================================================================

alias gq='git branch -v'
alias gs='git status'

function gl() {
    git log $1 --pretty=format:'%C(bold red)* %Creset%C(bold blue)%h%  %Creset%C(dim white)%<(15,trunc)%aN%Creset  %<(80,trunc)%s' -15
}

function gl1() {
    git log $1 --pretty=format:'%C(bold red)* %Creset%C(bold blue)%h%  %Creset%C(dim white)%<(15,trunc)%aN%Creset  %<(80,trunc)%s'
}


# ==================================================================================
# Google Cloud
# ==================================================================================
alias gg='cd $GOPATH'

export PATH=$PATH:~/code/mylib/flutter/bin:~/code/mylib/homebrew/bin
export PATH="$PATH":"$HOME/.pub-cache/bin"
export GOPATH=~/code/mylib/go

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/pwc/code/mylib/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/pwc/code/mylib/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/pwc/code/mylib/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/pwc/code/mylib/google-cloud-sdk/completion.zsh.inc'; fi

# POSTGRESQL
export PATH=$PATH:/Applications/Postgres.app/Contents/Versions/latest/bin

# DART
export PATH=$PATH:/Users/pwc/code/mylib/homebrew/opt/dart/libexec

