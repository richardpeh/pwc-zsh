Summary
-------

* Soft links the dotfiles 

    * `ln -s ~/pwc-zsh/zprofile ~/.zprofile`

    * `ln -s ~/pwc-zsh/zshrc ~/.zshrc`

* Make sure `~/.zshrc` ZSH env variable is set to `./ohmyzsh`


ZSH
---


## Manual Installation

1. Clone the repository:
>git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh

2. Optionally, backup your existing ~/.zshrc file:
>cp ~/.zshrc ~/.zshrc.orig

3. Create a new zsh configuration file
You can create a new zsh config file by copying the template that we have included for you.
>cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc

4. Change your default shell
>chsh -s $(which zsh)
You must log out from your user session and log back in to see this change.

5. Initialize your new zsh configuration

Once you open up a new terminal window, it should load zsh with Oh My Zsh's configuration.

## Installation Problems

If you have any hiccups installing, here are a few common fixes.

You might need to modify your PATH in ~/.zshrc if you're not able to find some commands after switching to oh-my-zsh.
If you installed manually or changed the install location, check the ZSH environment variable in ~/.zshrc.




## Using another customization directory

If you don't want to use the built-in custom directory itself, just change the path of $ZSH_CUSTOM inside your .zshrc to a directory of your own liking. Everything will be fine as long as you adhere to the conventional file hierarchy.

~/.zshrc

ZSH_CUSTOM=$HOME/my_customizations
File tree inside of your home directory:
```
$HOME
└── my_customizations
    ├── my_lib_patches.zsh
    ├── plugins
    │   └── my_plugin
    │       └── my_plugin.plugin.zsh
    └── themes
        └── my_awesome_theme.zsh-theme
```
